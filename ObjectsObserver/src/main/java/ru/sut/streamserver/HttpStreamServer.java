package ru.sut.streamserver;


import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpStreamServer implements Runnable {

    private final int port;
    private ServerSocket serverSocket;
    private Socket socket;
    private static final String BOUNDARY = "stream";
    private Mat imag;

    public HttpStreamServer(int port, Mat imagFr) {
        this.port = port;
        this.imag = imagFr;
    }

    private void startStreamingServer() throws IOException {
        serverSocket = new ServerSocket(port);
        socket = serverSocket.accept();
        writeHeader(socket.getOutputStream());
    }

    private void writeHeader(OutputStream stream) throws IOException {
        stream.write(("HTTP/1.0 200 OK\r\n" +
                "Connection: close\r\n" +
                "Max-Age: 0\r\n" +
                "Expires: 0\r\n" +
                "Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0\r\n" +
                "Pragma: no-cache\r\n" +
                "Content-Type: multipart/x-mixed-replace; " +
                "boundary=" + BOUNDARY + "\r\n" +
                "\r\n" +
                "--" + BOUNDARY + "\r\n").getBytes());
    }

    private void pushImage(Mat frame) throws IOException {
        if (frame == null)
            return;
        try {
            OutputStream outputStream = socket.getOutputStream();
            BufferedImage img = mat2bufferedImage(frame);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imageBytes = baos.toByteArray();
            outputStream.write(("Content-type: image/jpeg\r\n" +
                    "Content-Length: " + imageBytes.length + "\r\n" +
                    "\r\n").getBytes());
            outputStream.write(imageBytes);
            outputStream.write(("\r\n--" + BOUNDARY + "\r\n").getBytes());
        } catch (Exception ex) {
            socket = serverSocket.accept();
            writeHeader(socket.getOutputStream());
        }
    }


    public void run() {
        try {
            startStreamingServer();

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    pushImage(imag);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            stopStreamingServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateFrame(Mat imag) {
        this.imag = imag;
    }

    private void stopStreamingServer() throws IOException {
        socket.close();
        serverSocket.close();
    }

    private static BufferedImage mat2bufferedImage(Mat image) throws IOException {
        MatOfByte bytemat = new MatOfByte();
        Imgcodecs.imencode(".jpg", image, bytemat);
        byte[] bytes = bytemat.toArray();
        InputStream in = new ByteArrayInputStream(bytes);
        return ImageIO.read(in);
    }
}