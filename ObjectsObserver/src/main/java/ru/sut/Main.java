package ru.sut;

import org.opencv.core.Core;
import ru.sut.camera.AbstractVideoReader;
import ru.sut.camera.impl.Camera;
import ru.sut.camera.impl.VideoFileReader;
import ru.sut.configuration.json.Config;
import ru.sut.configuration.ConfigurationLoader;
import ru.sut.research.Researcher;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Optional;

/**
 * Для исследования видео необходимо добавить аргументы: video {путь к файлу}
 */
public class Main{

    private static Config config;
    private static Researcher researcher;

    static{
        try {
            config = ConfigurationLoader.getInstance().loadConfig();
            setLibraryPath(config.getPathToNativeLibs());
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        } catch (NoSuchFieldException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void setLibraryPath(String path) throws NoSuchFieldException, IllegalAccessException {
        System.setProperty("java.library.path", path);

        //set sys_paths to null
        final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
        sysPathsField.setAccessible(true);
        sysPathsField.set(null, null);
    }

    public static void main(String[] args) {
        AbstractVideoReader topVideo = null;
        AbstractVideoReader sideVideo = null;
        if (args == null || args.length == 0) {
            if (config.getSideCamera().isEnabled()) {
                sideVideo = new Camera(false, config.getSideCamera().getIndex());
                sideVideo.setAddingNewPointsToTrajectory(config.isDrawTrajectory());
            }
            if (config.getTopCamera().isEnabled()) {
                topVideo = new Camera(true, config.getTopCamera().getIndex());
                topVideo.setAddingNewPointsToTrajectory(config.isDrawTrajectory());
            }
        } else if (args.length >= 2 && "video".equalsIgnoreCase(args[0])) {
            sideVideo = new VideoFileReader(false, args[1]);
            sideVideo.setAddingNewPointsToTrajectory(config.isDrawTrajectory());
        }

        if (topVideo == null && sideVideo == null) {
            return;
        }

        Thread thread1 = null;
        if (topVideo != null) {
            thread1 = new Thread(topVideo);
            thread1.setDaemon(true);
        }

        Thread thread2 = null;
        if (sideVideo != null) {
            thread2 = new Thread(sideVideo);
            thread2.setDaemon(true);
        }

        Optional.ofNullable(thread1).ifPresent(Thread::start);
        Optional.ofNullable(thread2).ifPresent(Thread::start);

        while (sideVideo != null && (sideVideo.getMainObject() == null || sideVideo.getMainObject().getCenterPoint() == null)
                || topVideo != null && (topVideo.getMainObject() == null || topVideo.getMainObject().getCenterPoint() == null)){
            System.out.println("...");
        }


        System.out.println("OK");


        researcher = new Researcher(topVideo, sideVideo);
        researcher.start();

    }

    public Researcher getResearcher() {
        return researcher;
    }
}
