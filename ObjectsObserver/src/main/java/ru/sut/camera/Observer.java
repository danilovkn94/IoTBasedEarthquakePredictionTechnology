package ru.sut.camera;



public interface Observer {

    void updateScalars(double[] minValues, double[] maxValues);

}
