package ru.sut.camera;


import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import ru.sut.configuration.json.CameraConfig;
import ru.sut.configuration.json.CameraScales;
import ru.sut.configuration.ConfigurationLoader;
import ru.sut.camera.handlers.ImageHandler;
import ru.sut.camera.handlers.objects.MainObject;
import ru.sut.camera.handlers.objects.CameraTrajectory;
import ru.sut.streamserver.HttpStreamServer;

import java.io.IOException;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;


public abstract class AbstractVideoReader implements Runnable, Observer {

    private boolean isTopCamera;
    private ImageHandler imageHandler;
    private VideoCapture videoCapture = new VideoCapture();

    private Scalar minValues = new Scalar(0, 50, 0);
    private Scalar maxValues = new Scalar(20, 255, 255);

    private boolean addingNewPointsToTrajectory = false;

    private HttpStreamServer httpStreamService;

    private Mat frameForResultImage;
    private Mat frame;

    public AbstractVideoReader(boolean isTopCamera) {
        this.isTopCamera = isTopCamera;
    }

    @Override
    public void run() {

        frame = new Mat();
        CameraConfig cameraConfig = isTopCamera
                ? ConfigurationLoader.getInstance().getConfig().getTopCamera()
                : ConfigurationLoader.getInstance().getConfig().getSideCamera();

        initVideoCapture(videoCapture);

        int height = (int) videoCapture.get(Videoio.CV_CAP_PROP_FRAME_HEIGHT);
        int width = (int) videoCapture.get(Videoio.CV_CAP_PROP_FRAME_WIDTH);

        this.imageHandler = new ImageHandler(isTopCamera, height, width);

        Thread streamServerThread = null;
        if (cameraConfig.isServerEnabled()) {
            httpStreamService = new HttpStreamServer(cameraConfig.getServerPort(), frame);
            streamServerThread = new Thread(httpStreamService);
            streamServerThread.start();
        }

        Timer configUpdateTask = new Timer();
        configUpdateTask.scheduleAtFixedRate(getScalesLoaderTask(), 0, 500);

        boolean readSuccess;
        while (!Thread.interrupted()){

            readSuccess = videoCapture.read(frame);
            if (readSuccess) {
                Mat morphFrame = getMorphFrame(frame);
                drawContoursAndLines(morphFrame);
                httpStreamService.updateFrame(frameForResultImage);
            }
        }

        Optional.ofNullable(streamServerThread).ifPresent(Thread::interrupt);
        configUpdateTask.cancel();
        videoCapture.release();
    }

    /**
     * Updates minimum and maximum scalars for creating a mask frame
     */
    @Override
    public void updateScalars(double[] minValues, double[] maxValues) {
        this.minValues.set(minValues);
        this.maxValues.set(maxValues);
    }

    /**
     * Deletes trajectory of object
     */
    public void resetLine(){

        imageHandler.initNewLineTrajectory();
    }

    /**
     *Determines whether or not to draw a trajectory
     */
    public void setAddingNewPointsToTrajectory(boolean addingNewPointsToTrajectory) {
        this.addingNewPointsToTrajectory = addingNewPointsToTrajectory;
    }

    protected abstract void initVideoCapture(VideoCapture videoCapture);

    /**
     * Handles photo to get morphological photo with objects.
     */
    private Mat getMorphFrame(Mat frame){

        imageHandler.setFrame(frame);

        Mat hsvFrame = imageHandler.getHsvFrame();
        Mat maskFrame = imageHandler.getMaskFrame(hsvFrame, minValues, maxValues);

        return imageHandler.getMorphFrame(maskFrame);
    }

    /**
     * Draws contours and trajectory on main photo.
     */
    private void drawContoursAndLines(Mat morphFrame){
        imageHandler.drawLines(morphFrame, addingNewPointsToTrajectory);
        frameForResultImage = frame.clone();
    }

    public CameraTrajectory getTrajectory(){
        return imageHandler.getCameraTrajectory();
    }

    public MainObject getMainObject(){
        return imageHandler != null ? imageHandler.getMainObject() : null;
    }

    private TimerTask getScalesLoaderTask() {
        return new TimerTask(){

            @Override
            public void run() {
                CameraScales cameraScales;
                try {
                    if (isTopCamera) {
                        cameraScales = ConfigurationLoader.getInstance().loadTopCameraScales();
                    } else {
                        cameraScales = ConfigurationLoader.getInstance().loadSideCameraScales();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }

                if (cameraScales != null) {
                    double[] min = {cameraScales.getHueMin(), cameraScales.getSaturationMin(), cameraScales.getValueMin()};
                    double[] max = {cameraScales.getHueMax(), cameraScales.getSaturationMax(), cameraScales.getValueMax()};
                    updateScalars(min, max);
                }
            }
        };
    }
}
