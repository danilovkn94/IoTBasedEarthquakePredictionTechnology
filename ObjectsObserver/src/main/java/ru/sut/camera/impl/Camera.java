package ru.sut.camera.impl;

import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import ru.sut.camera.AbstractVideoReader;

public class Camera extends AbstractVideoReader {

    private static final int HEIGHT = 480;
    private static final int WIDTH = 720;

    private int cameraIndex;

    public Camera(boolean isTopCamera, int cameraIndex) {
        super(isTopCamera);
        this.cameraIndex = cameraIndex;
    }

    @Override
    protected void initVideoCapture(VideoCapture videoCapture) {
        if (videoCapture == null) {
            System.out.println("Camera initVideoCapture: videoCapture is null");
            return;
        }

        videoCapture.open(cameraIndex);

        videoCapture.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, WIDTH);
        videoCapture.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, HEIGHT);
    }
}
