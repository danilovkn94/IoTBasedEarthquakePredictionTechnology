package ru.sut.camera.impl;

import org.opencv.videoio.VideoCapture;
import ru.sut.camera.AbstractVideoReader;

public class VideoFileReader extends AbstractVideoReader {

    private String filename;

    public VideoFileReader(boolean isTopCamera, String fileName) {
        super(isTopCamera);
        this.filename = fileName;
    }

    @Override
    protected void initVideoCapture(VideoCapture videoCapture) {
        videoCapture.open(filename);
    }
}
