package ru.sut.camera.handlers.objects;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

public class ContourTop {

    private List<MatOfPoint> lines = new ArrayList<>();

    public ContourTop(){

        List<Point> pointsOfContour = new ArrayList<>();

        Point point1 = new Point(80, 100);
        pointsOfContour.add(point1);

        Point point2 = new Point(80, 350);
        pointsOfContour.add(point2);

        Point point3 = new Point(550, 350);
        pointsOfContour.add(point3);

        Point point4 = new Point(550, 100);
        pointsOfContour.add(point4);


        MatOfPoint contourVan = new MatOfPoint();
        contourVan.fromList(pointsOfContour);

        lines.add(contourVan);

    }

    public List<MatOfPoint> getLines() {
        return lines;
    }
}
