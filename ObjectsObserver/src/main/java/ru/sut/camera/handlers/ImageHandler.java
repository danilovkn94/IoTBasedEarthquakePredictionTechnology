package ru.sut.camera.handlers;


import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import ru.sut.camera.handlers.objects.*;

import java.util.ArrayList;
import java.util.List;

public class ImageHandler {

    private boolean isTopCamera;

    private static Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(16,16));
    private static Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(8, 8));

    private Mat frame;
    private Mat bufBlur;
    private Mat bufHsv;
    private Mat bufMask;
    private Mat bufMorph;

    private MainObject mainObject;
    private CameraTrajectory cameraTrajectory;

    private MainObjectDetector objectDetector;

    private Size blurSize;

    private Scalar objectColour;

    private int height;
    private int width;

    private ImageHandler(){

    }

    public ImageHandler(boolean isTopCamera, int height, int width) {

        this.isTopCamera = isTopCamera;

        bufBlur = new Mat();
        bufHsv = new Mat();
        bufMask = new Mat();
        bufMorph = new Mat();

        initNewLineTrajectory();

        blurSize = new Size(12, 12);
        objectColour = new Scalar(250, 0, 0);
        mainObject = new MainObject();

        objectDetector = new MainObjectDetector();

        this.height = height;
        this.width = width;
    }

    public Mat getMaskFrame(Mat hsvFrame, Scalar minValues, Scalar maxValues){

        Core.inRange(hsvFrame, minValues, maxValues, bufMask);

        return bufMask;
    }

    public Mat getMorphFrame(Mat mask){

        Imgproc.erode(mask, bufMorph, erodeElement);
        Imgproc.erode(mask, bufMorph, erodeElement);

        Imgproc.dilate(mask, bufMorph, dilateElement);
        Imgproc.dilate(mask, bufMorph, dilateElement);

        return bufMorph;
    }

    public void drawLines(Mat mask, boolean isAddingNewPoints){

        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Mat bufMask = mask.clone();

        Imgproc.findContours(bufMask, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

        if (hierarchy.size().height > 0 && hierarchy.size().width > 0){
            objectDetector.setContours(contours);
            objectDetector.updateMainObject(mainObject);

            Imgproc.drawMarker(frame, mainObject.getCenterPoint(), objectColour, Imgproc.MARKER_TILTED_CROSS, 30, 2, 2);
            Imgproc.polylines(frame, mainObject.getContourLine(), true, objectColour, 3);

            if (isAddingNewPoints)
                cameraTrajectory.addNewPointToLine(mainObject.getCenterPoint());
        }

        Imgproc.polylines(frame, cameraTrajectory.getLine(), false, objectColour, 1);
    }

    public void initNewLineTrajectory(){
        cameraTrajectory = new CameraTrajectory();
    }

    public Mat getTrajectoryMat(Mat frame){
        Imgproc.polylines(frame, cameraTrajectory.getLine(), false, objectColour, 1);
        return frame;
    }

    public Mat getTrajectoryMat(){
        Mat mat = new Mat(height, width, frame.type(), new Scalar(255, 255, 255));

        Imgproc.polylines(mat, cameraTrajectory.getLine(), false, objectColour, 1);
        return mat;
    }

    public Mat getHsvFrame(){

//        Imgproc.medianBlur(frame, frame, 7);
//        Imgproc.bilateralFilter(frame.clone(), frame, 7, 35, 35);

        Imgproc.blur(frame, bufBlur, blurSize);
        Imgproc.cvtColor(bufBlur, bufHsv, Imgproc.COLOR_BGR2HSV);

        return bufHsv;
    }

    public void setFrame(Mat frame) {
        Core.flip(frame, frame, 1);

        this.frame = frame;
    }

    public CameraTrajectory getCameraTrajectory() {
        return cameraTrajectory;
    }

    public MainObject getMainObject() {
        return mainObject;
    }
}
