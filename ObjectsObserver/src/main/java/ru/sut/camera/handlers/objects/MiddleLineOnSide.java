package ru.sut.camera.handlers.objects;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

public class MiddleLineOnSide {

    private List<MatOfPoint> line = new ArrayList<>();

    public MiddleLineOnSide(){
        Point point5 = new Point(80, 230);
        Point point6 = new Point(585, 230);


        List<Point> pointsOfMidLine = new ArrayList<>();
        pointsOfMidLine.add(point5);
        pointsOfMidLine.add(point6);

        MatOfPoint midLine = new MatOfPoint();
        midLine.fromList(pointsOfMidLine);

        line.add(midLine);
    }

    public List<MatOfPoint> getLine() {
        return line;
    }
}
