package ru.sut.camera.handlers.objects;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

public class CameraTrajectory {

    private List<Point> pointsOfLine;
    private MatOfPoint trajectoryPoints;
    private List<MatOfPoint> line;
    private Point lastPoint;
    private double length;

    public CameraTrajectory(){
        pointsOfLine = new ArrayList<>();
        trajectoryPoints = new MatOfPoint();
        line = new ArrayList<>();

        line.add(trajectoryPoints);

        length = 0;
    }

    public void addNewPointToLine(Point point){

        if (lastPoint != null) updateLength(point);

        lastPoint = point;

        pointsOfLine.add(point);
        trajectoryPoints.fromList(pointsOfLine);
    }

    private void updateLength(Point point){

        length += Math.sqrt(Math.pow((point.x - lastPoint.x), 2) + Math.pow((point.y - lastPoint.y), 2));

    }

    public List<MatOfPoint> getLine() {
        return line;
    }

    public double getLength() {
        return length;
    }

    public Point getLastPoint() {
        return lastPoint;
    }
}
