package ru.sut.camera.handlers.objects;


import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

public class ContourSide {

    private List<MatOfPoint> lines = new ArrayList<>();

    public ContourSide(){

        List<Point> pointsOfContour = new ArrayList<>();

        Point point1 = new Point(60, 20);
        pointsOfContour.add(point1);

        Point point2 = new Point(100, 450);
        pointsOfContour.add(point2);

        Point point3 = new Point(560, 450);
        pointsOfContour.add(point3);

        Point point4 = new Point(610, 20);
        pointsOfContour.add(point4);


        MatOfPoint contourVan = new MatOfPoint();
        contourVan.fromList(pointsOfContour);

        lines.add(contourVan);
    }

    public List<MatOfPoint> getLines() {
        return lines;
    }
}
