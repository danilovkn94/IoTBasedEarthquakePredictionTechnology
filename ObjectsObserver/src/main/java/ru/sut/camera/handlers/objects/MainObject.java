package ru.sut.camera.handlers.objects;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

public class MainObject {

    private Point centerPoint;
    private Point[] points;
    private List<MatOfPoint> contourLine;

    public MainObject(){

        contourLine = new ArrayList<>();

    }

    public List<MatOfPoint> getContourLine() {
        return contourLine;
    }

    public void setContourLine(MatOfPoint line) {
        contourLine.clear();
        contourLine.add(line);
    }

    public Point getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(Point centerPoint) {
        this.centerPoint = centerPoint;
    }

    public Point[] getPoints() {
        return points;
    }

    public void setPoints(Point[] points) {
        this.points = points;
    }
}
