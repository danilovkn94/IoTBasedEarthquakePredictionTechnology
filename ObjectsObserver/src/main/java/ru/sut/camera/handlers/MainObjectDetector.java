package ru.sut.camera.handlers;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import ru.sut.camera.handlers.objects.MainObject;

import java.util.List;

/**
 * The class that must find main researchable object.
 * Main researchable object is the object with the biggest contour.
 */
class MainObjectDetector {

    private List<MatOfPoint> contours;


    MainObjectDetector(){

    }

    void updateMainObject(MainObject mainObject){
        defineMainObject(mainObject);
        defineCenterPoint(mainObject);
    }

    private void defineMainObject(MainObject mainObject){
        MatOfPoint objectContour = contours.get(0);
        int s = objectContour.width() * objectContour.height();

        for (int i = 1; i < contours.size(); i++){
            int nextS = contours.get(i).width() * contours.get(i).height();
            if (s < nextS) {
                objectContour = contours.get(i);
                s = nextS;
            }
        }

        mainObject.setPoints(objectContour.toArray());
        mainObject.setContourLine(objectContour);
    }

    private void defineCenterPoint(MainObject mainObject){
        Point[] points = mainObject.getPoints();

        double xMin = Double.MAX_VALUE;
        double xMax = 0;

        double yMin = Double.MAX_VALUE;
        double yMax = 0;

        for (Point point : points) {
            if (xMin > point.x) xMin = point.x;
            if (xMax < point.x) xMax = point.x;

            if (yMin > point.y) yMin = point.y;
            if (yMax < point.y) yMax = point.y;
        }

        double x = (xMax + xMin) / 2;
        double y = (yMax + yMin) / 2;

        mainObject.setCenterPoint(new Point(x, y));
    }

    void setContours(List<MatOfPoint> contours) {
        this.contours = contours;
    }
}
