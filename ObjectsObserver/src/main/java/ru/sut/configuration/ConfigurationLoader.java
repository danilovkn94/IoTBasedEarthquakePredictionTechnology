package ru.sut.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.sut.configuration.json.CameraScales;
import ru.sut.configuration.json.Config;

import java.io.File;
import java.io.IOException;

public class ConfigurationLoader {

    private static final String CONFIG_PATH = "config/config.json";
    private static final String TOP_CAMERA_SCALES_PATH = "config/topCameraScales.json";
    private static final String SIDE_CAMERA_SCALES_PATH = "config/sideCameraScales.json";

    private static ConfigurationLoader instance;

    private ObjectMapper objectMapper;
    private Config config;

    private ConfigurationLoader() {
        objectMapper = new ObjectMapper();
    }

    public Config loadConfig() throws IOException {
        config = objectMapper.readValue(new File(CONFIG_PATH), Config.class);
        return config;
    }

    public CameraScales loadTopCameraScales() throws IOException {
        return objectMapper.readValue(new File(TOP_CAMERA_SCALES_PATH), CameraScales.class);
    }

    public CameraScales loadSideCameraScales() throws IOException {
        return objectMapper.readValue(new File(SIDE_CAMERA_SCALES_PATH), CameraScales.class);
    }

    public Config getConfig() {
        return config;
    }

    public static synchronized ConfigurationLoader getInstance() {
        if (instance == null) {
            instance = new ConfigurationLoader();
        }

        return instance;
    }
}
