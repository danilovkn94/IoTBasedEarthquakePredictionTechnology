package ru.sut.configuration.json;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Config {

    private String pathToNativeLibs;
    private CameraConfig topCamera;
    private CameraConfig sideCamera;
    private boolean drawTrajectory;
    private boolean saveToFile;
    private long pointPerTime;

    @JsonCreator
    public Config(@JsonProperty("pathToNativeLibs") String pathToNativeLibs, @JsonProperty("topCamera") CameraConfig topCamera,
                  @JsonProperty("sideCamera") CameraConfig sideCamera, @JsonProperty("drawTrajectory") boolean drawTrajectory,
                  @JsonProperty("saveToFile") boolean saveToFile, @JsonProperty("pointPerTime") long pointPerTime) {
        this.pathToNativeLibs = pathToNativeLibs;
        this.topCamera = topCamera;
        this.sideCamera = sideCamera;
        this.drawTrajectory = drawTrajectory;
        this.saveToFile = saveToFile;
        this.pointPerTime = pointPerTime;
    }

    public String getPathToNativeLibs() {
        return pathToNativeLibs;
    }

    public CameraConfig getTopCamera() {
        return topCamera;
    }

    public CameraConfig getSideCamera() {
        return sideCamera;
    }

    public boolean isDrawTrajectory() {
        return drawTrajectory;
    }

    public boolean isSaveToFile() {
        return saveToFile;
    }

    public long getPointPerTime() {
        return pointPerTime;
    }
}
