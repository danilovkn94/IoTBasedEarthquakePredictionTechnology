package ru.sut.configuration.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CameraConfig {

    private boolean enabled;
    private int index;
    private boolean serverEnabled;
    private int serverPort;

    @JsonCreator
    public CameraConfig(@JsonProperty("enabled") boolean enabled, @JsonProperty("index") int index,
                        @JsonProperty("serverEnabled") boolean serverEnabled, @JsonProperty("serverPort") int serverPort) {
        this.enabled = enabled;
        this.index = index;
        this.serverEnabled = serverEnabled;
        this.serverPort = serverPort;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getIndex() {
        return index;
    }

    public boolean isServerEnabled() {
        return serverEnabled;
    }

    public int getServerPort() {
        return serverPort;
    }
}
