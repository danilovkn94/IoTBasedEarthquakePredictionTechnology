package ru.sut.configuration.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Конфигураторция искомого объекта.
 * Hue, Saturation, Value — тон(0 - 360), насыщенность(0 - 255), значение цвета(0 - 255)
 */
public class CameraScales {

    private double hueMin;
    private double hueMax;
    private double saturationMin;
    private double saturationMax;
    private double valueMin;
    private double valueMax;

    @JsonCreator
    public CameraScales(@JsonProperty("hueMin") double hueMin, @JsonProperty("hueMax") double hueMax,
                        @JsonProperty("saturationMin") double saturationMin, @JsonProperty("saturationMax") double saturationMax,
                        @JsonProperty("valueMin") double valueMin, @JsonProperty("valueMax") double valueMax) {
        this.hueMin = hueMin;
        this.hueMax = hueMax;
        this.saturationMin = saturationMin;
        this.saturationMax = saturationMax;
        this.valueMin = valueMin;
        this.valueMax = valueMax;
    }

    public double getHueMin() {
        return hueMin;
    }

    public double getHueMax() {
        return hueMax;
    }

    public double getSaturationMin() {
        return saturationMin;
    }

    public double getSaturationMax() {
        return saturationMax;
    }

    public double getValueMin() {
        return valueMin;
    }

    public double getValueMax() {
        return valueMax;
    }
}
