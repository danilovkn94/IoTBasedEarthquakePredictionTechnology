package ru.sut.research;


import ru.sut.camera.AbstractVideoReader;
import ru.sut.research.distance.DistanceCounter;
import ru.sut.research.trajectory.TrajectoryCreator;

import java.util.Timer;
import java.util.TimerTask;

public class Researcher {

    private DistanceCounter distanceCounter;
    private TrajectoryCreator trajectoryCreator;

    private AbstractVideoReader topCamera;
    private AbstractVideoReader sideCamera;

    private Timer timer;

    public Researcher(AbstractVideoReader topCamera, AbstractVideoReader sideCamera) {
        this.topCamera = topCamera;
        this.sideCamera = sideCamera;
        distanceCounter = new DistanceCounter(sideCamera);
        trajectoryCreator = new TrajectoryCreator(topCamera, sideCamera);
    }

    public void start(){
        distanceCounter.start();
        trajectoryCreator.start();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Central point: " + sideCamera.getMainObject().getCenterPoint());
                System.out.println("Distance: " + distanceCounter.getLength());
            }
        }, 0, 500);
    }

    public void stop(){
        trajectoryCreator.stop();
        distanceCounter.stop();
    }

    public void reset(){
        trajectoryCreator.reset();
        distanceCounter.reset();
    }
}
