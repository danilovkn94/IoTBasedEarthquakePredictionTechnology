package ru.sut.research.distance;

import ru.sut.camera.AbstractVideoReader;

import java.util.Timer;

public class DistanceCounter {

    private AbstractVideoReader sideCamera;

    private DistanceTimerTask distanceTimerTask;

    private Timer timer;
    private long timerInterval;

    public DistanceCounter(AbstractVideoReader sideCamera) {
        this.sideCamera = sideCamera;

        timerInterval = 100;

        distanceTimerTask = new DistanceTimerTask(sideCamera);
    }

    public void reset(){

        try {
            timer.cancel();
            timer.purge();
        } catch (Exception ignored){

        }

        distanceTimerTask = new DistanceTimerTask(sideCamera);
    }

    public void start(){
        timer = new Timer(true);
        timer.scheduleAtFixedRate(distanceTimerTask, 0, timerInterval);
    }

    public void stop(){
        timer.cancel();
        distanceTimerTask = new DistanceTimerTask(sideCamera, distanceTimerTask.getLength());
    }

    public double getLength() {
        if (distanceTimerTask == null) {
            return 0.0;
        }

        return distanceTimerTask.getLength();
    }
}
