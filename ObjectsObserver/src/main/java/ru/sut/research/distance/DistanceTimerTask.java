package ru.sut.research.distance;

import javafx.geometry.Point2D;
import ru.sut.camera.AbstractVideoReader;

import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;

public class DistanceTimerTask extends TimerTask{

    private static final double PIXEL_LENGTH = 190.0 / 505.0;

    private AbstractVideoReader sideCamera;

    private AtomicLong length;

    private Point2D currentPoint;
    private Point2D lastPoint;

    public DistanceTimerTask(AbstractVideoReader sideCamera) {
        this.sideCamera = sideCamera;

        this.length = new AtomicLong(0);
    }

    public DistanceTimerTask(AbstractVideoReader sideCamera, double length) {
        this.sideCamera = sideCamera;
        this.length = new AtomicLong(Double.doubleToLongBits(length));
    }

    @Override
    public void run() {

        updateLength();
    }

    private void updateLength(){

        if (sideCamera.getTrajectory().getLastPoint() == null){
            return;
        }

        if (currentPoint == null){
            currentPoint = new Point2D(sideCamera.getTrajectory().getLastPoint().x,
                    sideCamera.getTrajectory().getLastPoint().y);
        } else {
            lastPoint = currentPoint;
            currentPoint = new Point2D(sideCamera.getTrajectory().getLastPoint().x,
                    sideCamera.getTrajectory().getLastPoint().y);

            length.set(Double.doubleToLongBits(currentPoint.distance(lastPoint)));
        }
    }

    public double getLength() {
        return Double.longBitsToDouble(length.get());
    }
}
