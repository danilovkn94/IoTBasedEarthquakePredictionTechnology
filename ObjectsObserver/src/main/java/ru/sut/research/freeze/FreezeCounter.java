package ru.sut.research.freeze;

import ru.sut.camera.AbstractVideoReader;

import java.util.Timer;

public class FreezeCounter{

    private AbstractVideoReader camera;

    private Timer timer;

    private long timerInterval;

    private FreezeTimerTask freezeTimerTask;

    public FreezeCounter(AbstractVideoReader camera) {
        this.camera = camera;
        this.timerInterval = 500;

        freezeTimerTask = new FreezeTimerTask(camera, timerInterval);
    }

    public void reset(){

        try {
            timer.cancel();
            timer.purge();
        } catch (Exception ignored){

        }

        freezeTimerTask = new FreezeTimerTask(camera, timerInterval);
    }

    public void start(){
        timer = new Timer(true);
        timer.scheduleAtFixedRate(freezeTimerTask, 0, timerInterval);
    }

    public void stop(){
        timer.cancel();
        freezeTimerTask = new FreezeTimerTask(camera, freezeTimerTask.getFreezeCount(),
                freezeTimerTask.getFreezeTime(), timerInterval);
    }

    public int getFreezeCount() {
        return freezeTimerTask.getFreezeCount();
    }

    public long getFreezeTime() {
        return freezeTimerTask.getFreezeTime();
    }
}
