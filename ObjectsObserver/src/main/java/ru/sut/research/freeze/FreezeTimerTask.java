package ru.sut.research.freeze;

import ru.sut.camera.AbstractVideoReader;

import java.util.TimerTask;

class FreezeTimerTask extends TimerTask{

    private static final int P_FOR_FREEZE = 15;

    private AbstractVideoReader camera1;

    private double sideLength;
    private double sideLengthPrev;

    private int freezeCount;
    private long freezeTime;

    private long timerInterval;

    private boolean isMoving;

    FreezeTimerTask(AbstractVideoReader camera1, long timerInterval) {
        this.camera1 = camera1;

        this.timerInterval = timerInterval;

        sideLength = 0;
        sideLengthPrev = 0;

        freezeCount = 0;
        freezeTime = 0;

        isMoving = false;
    }

    FreezeTimerTask(AbstractVideoReader camera1, int freezeCount,
                    long freezeTime, long timerInterval) {
        this.camera1 = camera1;

        this.freezeCount = freezeCount;
        this.freezeTime = freezeTime;
        this.timerInterval = timerInterval;

        sideLength = 0;
        sideLengthPrev = 0;

        isMoving = false;
    }

    @Override
    public void run() {

        sideLength = camera1.getTrajectory().getLength();

        if (Math.abs(sideLength - sideLengthPrev) > P_FOR_FREEZE)
        {
            isMoving = true;
        }
        else if (Math.abs(sideLength - sideLengthPrev) < P_FOR_FREEZE)
        {
            if (isMoving) {
                freezeCount++;
                freezeTime += timerInterval;
            }
            isMoving = false;
        }

        sideLengthPrev = sideLength;
    }

    int getFreezeCount() {
        return freezeCount;
    }

    long getFreezeTime() {
        return freezeTime;
    }
}
