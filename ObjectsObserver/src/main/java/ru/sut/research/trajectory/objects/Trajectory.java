package ru.sut.research.trajectory.objects;

import java.util.ArrayList;
import java.util.List;

public class Trajectory {

    private List<Point3d> points;

    public Trajectory() {
        points = new ArrayList<>();
    }

    public Trajectory(List<Point3d> points) {
        this.points = points;
    }

    public void addPointToTrajectory(Point3d point) {
        if (point != null) {
            points.add(point);
        }
    }

    public List<Point3d> getPoints() {
        return points;
    }
}
