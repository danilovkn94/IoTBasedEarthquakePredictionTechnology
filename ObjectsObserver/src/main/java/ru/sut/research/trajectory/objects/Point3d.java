package ru.sut.research.trajectory.objects;

import org.opencv.core.Point;
import org.opencv.core.Point3;

public class Point3d extends Point3 {
    public long time;

    public Point3d(Point p, long time) {
        super(p);
        this.time = time;
    }

    public Point3d(Point p, double z, long time) {
        super(p.x, p.y, z);
        this.time = time;
    }

    public Point3d(double x, double y, double z, long time) {
        super(x, y, z);
        this.time = time;
    }

    @Override
    public String toString() {
        return x + "," + y + "," + "," + z + "," + time;
    }
}
