package ru.sut.research.trajectory;

import ru.sut.camera.AbstractVideoReader;
import ru.sut.configuration.ConfigurationLoader;
import ru.sut.research.trajectory.objects.Trajectory;

import java.util.Timer;

public class TrajectoryCreator {

    private AbstractVideoReader topCamera;
    private AbstractVideoReader sideCamera;
    private TrajectoryCreatorTimerTask timerTask;
    private Timer timer;
    private long timerInterval;

    public TrajectoryCreator(AbstractVideoReader topCamera, AbstractVideoReader sideCamera) {
        this.topCamera = topCamera;
        this.sideCamera = sideCamera;
        timerInterval = ConfigurationLoader.getInstance().getConfig().getPointPerTime();
        timerTask = new TrajectoryCreatorTimerTask(topCamera, sideCamera);
    }

    public void reset(){

        try {
            timerTask.beforeCancel();
            timer.cancel();
            timer.purge();
        } catch (Exception ignored){

        }

        timerTask = new TrajectoryCreatorTimerTask(topCamera, sideCamera);
    }

    public void start(){
        timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask, 0, timerInterval);
    }

    public void stop(){
        if (timer != null) {
            timerTask.beforeCancel();
            timer.cancel();
            timerTask = new TrajectoryCreatorTimerTask(topCamera, sideCamera, timerTask.getTrajectory());
        }
    }

    public Trajectory getTrajectory() {
        if (timerTask == null) {
            return null;
        }

        return timerTask.getTrajectory();
    }
}
