package ru.sut.research.trajectory;

import org.opencv.core.Point;
import ru.sut.camera.AbstractVideoReader;
import ru.sut.configuration.ConfigurationLoader;
import ru.sut.research.trajectory.objects.Point3d;
import ru.sut.research.trajectory.objects.Trajectory;

import java.io.IOException;
import java.util.Optional;
import java.util.TimerTask;

public class TrajectoryCreatorTimerTask extends TimerTask {

    private AbstractVideoReader topCamera;
    private AbstractVideoReader sideCamera;
    private Trajectory trajectory;
    private WriterTrajectoryToCsv writer;

    public TrajectoryCreatorTimerTask(AbstractVideoReader topCamera, AbstractVideoReader sideCamera) {
        this.topCamera = topCamera;
        this.sideCamera = sideCamera;
        trajectory = new Trajectory();

        if (ConfigurationLoader.getInstance().getConfig().isDrawTrajectory() &&
                ConfigurationLoader.getInstance().getConfig().isSaveToFile()) {
            createWriter();
        }
    }

    public TrajectoryCreatorTimerTask(AbstractVideoReader topCamera, AbstractVideoReader sideCamera, Trajectory trajectory) {
        this.topCamera = topCamera;
        this.sideCamera = sideCamera;
        this.trajectory = trajectory;

        if (ConfigurationLoader.getInstance().getConfig().isDrawTrajectory() &&
                ConfigurationLoader.getInstance().getConfig().isSaveToFile()) {
            createWriter();
        }
    }

    @Override
    public void run() {
        if (sideCamera == null) {
            cancel();
            return;
        }

        Point sidePoint = sideCamera.getTrajectory().getLastPoint();
        if (sidePoint == null) {
            return;
        }

        Point3d point;
        if (topCamera != null) {
            point = new Point3d(sidePoint, topCamera.getTrajectory().getLastPoint().y, System.currentTimeMillis());
        } else {
            point = new Point3d(sidePoint, System.currentTimeMillis());
        }

        trajectory.addPointToTrajectory(point);

        Optional.ofNullable(writer).ifPresent(w -> w.writeData(point.x, point.y, point.z, point.time));
    }

    public void beforeCancel() {
        Optional.ofNullable(writer).ifPresent(WriterTrajectoryToCsv::close);
    }

    public Trajectory getTrajectory() {
        return trajectory;
    }

    private void createWriter() {
        try {
            writer = new WriterTrajectoryToCsv();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
