package ru.sut.research.trajectory;

import com.opencsv.CSVWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WriterTrajectoryToCsv {

    private static final String FILE_NAME = "trajectory";
    private static final String[] HEADER = {"x", "y", "z", "time"};
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

    private final CSVWriter writer;
    private OutputStreamWriter osw;

    public WriterTrajectoryToCsv() throws FileNotFoundException {
        FileOutputStream fos = new FileOutputStream(FILE_NAME + new Date().toString() + ".csv");
        osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        writer = new CSVWriter(osw);
        writer.writeNext(HEADER);
    }

    public void writeData(double x, double y, double z, long time) {
        writer.writeNext(new String[]{Double.toString(x), Double.toString(y), Double.toString(z), dateFormat.format(new Date(time))});
        try {
            osw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
