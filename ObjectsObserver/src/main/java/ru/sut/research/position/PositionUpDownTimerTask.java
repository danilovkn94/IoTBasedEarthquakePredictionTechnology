package ru.sut.research.position;

import ru.sut.camera.AbstractVideoReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;

public class PositionUpDownTimerTask extends TimerTask{

    private AbstractVideoReader sideCamera;
    private int count;
    private boolean isUp;
    private long interval;
    private long time;
    private List<Integer> migrationResultsPerMinute;
    private List<Double> timeUpResultsPerMinute;
    private List<Double> timeDownResultsPerMinute;
    private int result;

    private double upTime;
    private double downTime;

    private int minute;

    public PositionUpDownTimerTask(AbstractVideoReader sideCamera, long interval) {
        this.sideCamera = sideCamera;

        count = 0;
        result = 0;
        this.interval = interval;
        time = 0;
        migrationResultsPerMinute = new ArrayList<>();
        timeUpResultsPerMinute = new ArrayList<>();
        timeDownResultsPerMinute = new ArrayList<>();
        minute = 0;

        upTime = 0;
        downTime = 0;

        isUp = sideCamera.getMainObject().getCenterPoint().y < 230;
    }

    public PositionUpDownTimerTask(AbstractVideoReader sideCamera, long interval,
                                   List<Integer> migrationResultsPerMinute, List<Double> timeUpResultsPerMinute,
                                   List<Double> timeDownResultsPerMinute, long time)
    {
        this.sideCamera = sideCamera;

        this.interval = interval;
        this.time = time;
        this.migrationResultsPerMinute = migrationResultsPerMinute;
        this.timeUpResultsPerMinute = timeUpResultsPerMinute;
        this.timeDownResultsPerMinute = timeDownResultsPerMinute;
        minute = migrationResultsPerMinute.size() - 1;
        count = migrationResultsPerMinute.get(minute);

        upTime = timeUpResultsPerMinute.get(minute);
        downTime = timeDownResultsPerMinute.get(minute);

        result = 0;

        for (int r : migrationResultsPerMinute){
            result += r;
        }

        isUp = sideCamera.getMainObject().getCenterPoint().y < 230;
    }

    @Override
    public void run() {

        if (isUp && sideCamera.getMainObject().getCenterPoint().y > 230){
            count++;
            result++;
            isUp = false;
        } else if (!isUp && sideCamera.getMainObject().getCenterPoint().y < 230){
            isUp = true;
            count++;
            result++;
        }

        if (isUp) addTimeToUp();
        else addTimeToDown();

        time += interval;

        if (minute == migrationResultsPerMinute.size()) {
            migrationResultsPerMinute.add(minute, count);
        }
        else {
            migrationResultsPerMinute.set(minute, count);
        }

        if (time >= 60_000) {
            minute++;
            count = 0;
            time = 0;

            upTime = 0;
            downTime = 0;
        }
    }

    private void addTimeToUp(){

        upTime += (double) interval;

        if (minute == timeUpResultsPerMinute.size() && minute == timeDownResultsPerMinute.size()){
            timeUpResultsPerMinute.add(minute, upTime / 1000);
            timeDownResultsPerMinute.add(minute, downTime / 1000);
        }
        else timeUpResultsPerMinute.set(minute, upTime / 1000);
    }

    private void addTimeToDown(){

        downTime += (double) interval;

        if (minute == timeDownResultsPerMinute.size() && minute == timeUpResultsPerMinute.size()){
            timeDownResultsPerMinute.add(minute, downTime / 1000);
            timeUpResultsPerMinute.add(minute, upTime / 1000);
        }
        else timeDownResultsPerMinute.set(minute, downTime / 1000);
    }

    private String getInfoTime(List<Double> results){

        double result = 0;

        for (double r : results)
            result += r;

        return String.format(Locale.ENGLISH, results + " = %.1f s", result);
    }

    public int getResult() {
        return result;
    }

    public List<Integer> getMigrationResultsPerMinute() {
        return migrationResultsPerMinute;
    }

    public List<Double> getTimeUpResultsPerMinute() {
        return timeUpResultsPerMinute;
    }

    public List<Double> getTimeDownResultsPerMinute() {
        return timeDownResultsPerMinute;
    }

    public long getTime() {
        return time;
    }
}
