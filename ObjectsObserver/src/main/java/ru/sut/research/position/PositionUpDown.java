package ru.sut.research.position;

import ru.sut.camera.AbstractVideoReader;
import java.util.Timer;

public class PositionUpDown {

    private AbstractVideoReader sideCamera;
    private Timer timer;
    private PositionUpDownTimerTask timerTask;

    private long timerInterval;

    public PositionUpDown(AbstractVideoReader sideCamera) {
        this.sideCamera = sideCamera;
        timerInterval = 100;

        timerTask = new PositionUpDownTimerTask(sideCamera, timerInterval);
    }

    public void start(){
        timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask, 0, timerInterval);
    }

    public void stop(){
        timer.cancel();
        timerTask = new PositionUpDownTimerTask(sideCamera, timerInterval,
                timerTask.getMigrationResultsPerMinute(), timerTask.getTimeUpResultsPerMinute(),
                timerTask.getTimeDownResultsPerMinute(), timerTask.getTime());
    }

    public void reset(){
        try {
            timer.cancel();
            timer.purge();
        } catch (Exception ignored){

        }

        timerTask = new PositionUpDownTimerTask(sideCamera, timerInterval);

    }
}
